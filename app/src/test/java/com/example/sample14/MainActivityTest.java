package com.example.sample14;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MainActivityTest {

    MainActivity activity;

    @Before
    public void setUp() {
        activity = new MainActivity();
    }

    @Test
    public void testInvalidOperation() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x, y, 3), is("Invalid operation."));
    }

    @Test
    public void testInvalidFormat() {
        String x = "A";
        String y = "2.0";
        assertThat(activity.makeResult(x, y, 3), is("Invalid format."));
    }

    @Test
    public void testAdd() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x, y, R.id.radioButtonAddition), is("1.0 + 2.0 = 3.0"));
    }

    @Test
    public void testSub() {
        String x = "2.0";
        String y = "1.0";
        assertThat(activity.makeResult(x, y, R.id.radioButtonSubtraction), is("2.0 - 1.0 = 1.0"));
    }



    @Test
    public void testMul() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x, y, R.id.radioButtonMultiplication), is("1.0 * 2.0 = 2.0"));
    }
    @Test
    public void testDiv() {
        String x = "4.0";
        String y = "2.0";
        assertThat(activity.makeResult(x, y, R.id.radioButtonDivision), is("4.0 / 2.0 = 2.0"));
    }
}
